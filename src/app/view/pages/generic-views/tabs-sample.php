<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<section class="body">

    <div class="content">

        <div class="wrapper">

            <h1 class="segment-title text-center">Tabs</h1>

            <div data-tab-menu="tabs-1" class="tabs-menu-items">
                <div data-tab-active="yes" data-tab-target="tab-1">Tab 1</div>
                <div data-tab-active="no" data-tab-target="tab-2">Tab 2</div>
                <div data-tab-active="no" data-tab-target="tab-3">Tab 3</div>
            </div>

        </div>

    </div>

    <div data-tab-content="tabs-1" class="content">

        <div data-tab-name="tab-1" class="wrapper medium-size no-padding-top">
            <h3>#1 Lorem ipsum dolor sit amet.</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
        </div>
        <div data-tab-name="tab-2" class="wrapper medium-size">
            <h3>#2 Lorem ipsum dolor sit amet.</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
        </div>
        <div data-tab-name="tab-3" class="wrapper medium-size">
            <h3>#3 Lorem ipsum dolor sit amet.</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit provident, ab, quisquam fugit error sunt deserunt architecto blanditiis ipsum ut! Libero hic commodi distinctio obcaecati nisi numquam rem laboriosam?</p>
        </div>

    </div>

</section>
