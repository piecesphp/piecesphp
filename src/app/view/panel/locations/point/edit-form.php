<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<section class="module-view-container">

    <?php if(isset($breadcrumbs)): ?>
    <div class="breadcrumb">
        <?= $breadcrumbs ?>
    </div>
    <?php endif; ?>


    <div class="limiter-content">

        <div class="section-title">
            <div class="title"><?= __(LOCATIONS_LANG_GROUP, 'Editar'); ?> <?= $title; ?></div>
            <?php if(isset($description) && is_string($description) && mb_strlen(trim($description)) > 0): ?>
            <div class="description"><?= $description; ?></div>
            <?php endif; ?>
        </div>

        <br>

        <div class="container-standard-form">
            <form pcs-generic-handler-js method='POST' action="<?= $action;?>" class="ui form">

                <input type="hidden" name="id" value="<?=$element->id;?>">

                <div class="field required">
                    <label><?= __(LOCATIONS_LANG_GROUP, 'País'); ?></label>
                    <select required name="country" locations-component-auto-filled-country="<?=$element->city->state->country->id;?>"></select>
                </div>

                <div class="field required">
                    <label><?= __(LOCATIONS_LANG_GROUP, 'Departamento'); ?></label>
                    <select required name="state" locations-component-auto-filled-state="<?=$element->city->state->id;?>"></select>
                </div>

                <div class="field required">
                    <label><?= __(LOCATIONS_LANG_GROUP, 'Ciudad'); ?></label>
                    <select required name="city" locations-component-auto-filled-city="<?=$element->city->id;?>"></select>
                </div>

                <div class="field required">
                    <label><?= __(LOCATIONS_LANG_GROUP, 'Dirección'); ?> <small><?= __(LOCATIONS_LANG_GROUP, '(localidad, caserío, barrio, etc...)'); ?></small></label>
                    <input type="text" name="address" required value="<?=htmlentities($element->address);?>">
                </div>

                <div class="field required">
                    <label class=''><?= __(LOCATIONS_LANG_GROUP, 'Ubicación en el mapa de la localidad'); ?></label>
                    <input longitude-mapbox-handler name='longitude' type='hidden' required value="<?= $element->longitude;?>">
                    <input latitude-mapbox-handler name='latitude' type='hidden' required value="<?= $element->latitude;?>">
                </div>

                <div class="field">
                    <button set-satelital-view class="ui mini button red inverted"><?= __(LOCATIONS_LANG_GROUP, 'Vista satelital'); ?></button>
                    <button set-draw-view class="ui mini button red inverted"><?= __(LOCATIONS_LANG_GROUP, 'Vista de dibujo'); ?></button>
                    <button set-center-view class="ui mini button red inverted"><?= __(LOCATIONS_LANG_GROUP, 'Centrar'); ?></button>
                    <small><?= __(LOCATIONS_LANG_GROUP, '(para mayor precisión, puede mover el cursor de posición)'); ?></small>
                </div>

                <div class="field">
                    <div id="map">
                    </div>
                </div>

                <div class="field required">
                    <label><?= __(LOCATIONS_LANG_GROUP, 'Nombre'); ?></label>
                    <input type="text" name="name" maxlength="255" value="<?=htmlentities($element->name);?>">
                </div>

                <div class="field required">
                    <label><?= __(LOCATIONS_LANG_GROUP, 'Activo/Inactivo'); ?></label>
                    <select required name="active">
                        <?=$status_options;?>
                    </select>
                </div>

                <div class="field">
                    <button type="submit" class="ui button green"><?= __(LOCATIONS_LANG_GROUP, 'Guardar'); ?></button>
                </div>

            </form>
        </div>

    </div>

</section>
