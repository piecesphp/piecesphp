<div class="main-buttons">

	<div class="element">
		<button class="ui button red" type="button" cancel><?= $cancelButtonText; ?></button>
	</div>

	<div class="element">
		<button class="ui button green" type="button" save><?= $saveButtonText; ?></button>
	</div>

</div>
