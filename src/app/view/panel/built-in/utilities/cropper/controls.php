<div class="controls">
    <?php $this->_render('panel/built-in/utilities/cropper/inc/notes.php', [
        'notes' => $notes,
    ]);?>
    <?php $this->_render('panel/built-in/utilities/cropper/inc/options.php', [
        'controls' => $controls,
    ]);?>
</div>
