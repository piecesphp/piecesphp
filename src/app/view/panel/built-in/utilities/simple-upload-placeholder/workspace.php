<?php

$langGroup = 'NONE';
$buttonText = isset($buttonText) && is_string($buttonText) ? $buttonText : __($langGroup, 'Agregar archivo');
$inputNameAttr = isset($inputNameAttr) && is_string($inputNameAttr) ? $inputNameAttr : 'file';
$required = isset($required) && is_bool($required) ? $required : true;
$multiple = isset($multiple) && is_bool($multiple) ? $multiple : true;
$accept = isset($accept) && is_string($accept) ? $accept : null;
$icon = isset($icon) && is_string($icon) ? $icon : 'file outline';
$onlyButton = isset($onlyButton) && is_bool($onlyButton) ? $onlyButton : false;
$imagePreview = isset($imagePreview) && is_string($imagePreview) ? "'{$imagePreview}'" : null;
$classesButton = isset($classesButton) && is_string($classesButton) ? $classesButton : null;

?>

<div class="simple-upload-placeholder <?= $onlyButton ? 'only-button' : ''; ?>">
    <div class="preview">
        <div class="placeholder-icon">
            <i class="icon <?= $icon; ?>"></i>
        </div>
        <div class="overlay-element" style="<?= $imagePreview !== null ? "background-image: url($imagePreview);" : ''; ?>"></div>
    </div>
    <label file-label><?= $buttonText; ?></label>
    <input type="file" name="<?= $inputNameAttr; ?>" <?= $required ? 'required' : ''; ?> <?= $multiple ? 'multiple' : ''; ?> <?= $accept !== null ? "accept='{$accept}'" : ''; ?>>
    <button class="ui button trigger-file icon labeled<?= $classesButton !== null ? " $classesButton" : ''; ?>">
        <i class="icon upload"></i>
        <span class="text"><?= $buttonText; ?></span>
    </button>
</div>
