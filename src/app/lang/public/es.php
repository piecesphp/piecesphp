<?php

namespace App\LangMessages;

//Hereda los mensajes que estan en lang/(*).php
return [
    'CAPTCHA_FAIL' => 'No ha pasado la prueba captcha anti-spam, intente nuevamente refrescando la página.',
];
