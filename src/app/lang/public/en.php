<?php

namespace App\LangMessages;

//Hereda los mensajes que estan en lang/(*).php
return [
    'Publicaciones'                                                        => 'Publications',
    '{DAY_NUMBER} de {MONTH_NAME}, {YEAR}'                                 => '{MONTH_NAME} {DAY_NUMBER}, {YEAR}',
    'Mensaje'                                                              => 'Message',
    'El mensaje ha sido enviado.'                                          => 'The message has been sent.',
    'Ha ocurrido un error desconocido, intente más tarde.'                 => 'An unknown error has occurred, please try again later.',
    'Ha ocurrido un error desconocido al procesar los valores ingresados.' => 'An unknown error occurred while processing the entered values.',
    "Fue contactado desde: <a href='%s'>%s</a>"                            => "He was contacted from: <a href='%s'>% s </a>",
    'Contacto'                                                             => 'Contact',
    'Ejemplo de tabs'                                                      => 'Example of tabs',
    'Nombre'                                                               => 'Name',
    'Correo electrónico'                                                   => 'Email',
    'Marque si desea recibir actualizaciones a su correo electrónico'      => 'Check if you want to receive updates to your email',
    'Asunto'                                                               => 'Subject',
    'Enviar'                                                               => 'Send',
    'Página principal'                                                     => 'Homepage',
    'Blog'                                                                 => 'Blog',
    'Cargar más'                                                           => 'Load more',
    'Todos los derechos reservados'                                        => 'All rights reserved',
    'E-mail'                                                               => 'E-mail',
    'Acepta que le envíen actualizaciones'                                 => 'Agree to receive updates',
    'Sí'                                                                   => 'Yes',
    'No'                                                                   => 'Not',
    'Ver más'                                                              => 'See more',
    'Todas las categorías'                                                 => 'Of all categories',
    'Elementos'                                                            => 'Elements',
    'Idiomas'                                                              => 'Languages',
    'CAPTCHA_FAIL'                                                         => 'Failed the anti-spam captcha test, try again by refreshing the page.',
];
