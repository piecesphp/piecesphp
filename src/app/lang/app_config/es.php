<?php

return [
    'DESCRIPTION_COLOR_MENU_BACKGROUND' => 'Corresponde al color de fondo de las barras de menú, al modificar este color cambiara el color de fondo de todas las barras',
    'DESCRIPTION_COLOR_MENU_MARK'       => 'Corresponde al color de fondo de las barras de menú, al modificar este color cambiara el color de fondo de todas las barras',
    'DESCRIPTION_COLOR_MENU_FONT'       => 'Corresponde al color de iconos, fuentes y líneas decorativas de las barras de menú, al modificar este color cambiara el color de estos elementos en todas las barras',
];
