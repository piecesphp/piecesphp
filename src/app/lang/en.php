<?php

namespace App\LangMessages;

  //Hereda los mensajes que estan en lang/default.php
$lang = [
    'lang' => [
        'es' => 'Spanish',
        'en' => 'English',
        'fr' => 'French',
        'de' => 'German',
        'it' => 'Italian',
        'pt' => 'Portuguese',
    ],
    'langShort' => [
        'es' => 'ES',
        'en' => 'EN',
        'fr' => 'FR',
        'de' => 'DE',
        'it' => 'IT',
        'pt' => 'PT',
    ],
    'formatsDate' => [
        'd-m-Y h:i:s'   => 'Y-m-d h:i:s',
        'd-m-Y h:i:s A' => 'Y-m-d h:i:s A',
        'd-m-Y H:i:s'   => 'Y-m-d H:i:s',
    ],
    'day' => [
        0 => 'Sunday',
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday',
    ],
    'month' => [
        0  => 'January',
        1  => 'February',
        2  => 'March',
        3  => 'April',
        4  => 'May',
        5  => 'June',
        6  => 'July',
        7  => 'August',
        8  => 'September',
        9  => 'October',
        10 => 'November',
        11 => 'December',
    ],
    'calendar' => [
        'Enero'      => 'January',
        'Febrero'    => 'February',
        'Marzo'      => 'March',
        'Abril'      => 'April',
        'Mayo'       => 'May',
        'Junio'      => 'June',
        'Julio'      => 'July',
        'Agosto'     => 'August',
        'Septiembre' => 'September',
        'Octubre'    => 'October',
        'Noviembre'  => 'November',
        'Diciembre'  => 'December',
    ],
    'general' => [
        'messages'                   => 'Messages',
        'logout'                     => 'Logout',
        'password_recovery'          => 'Recovery password',
        'password_restored'          => 'New password',
        'verification_code'          => 'Verification codew',
        'active'                     => 'Active',
        'inactive'                   => 'Inactive',
        'select_element'             => 'Select an item',
        'users'                      => 'Users',
        'firstname'                  => 'First name',
        'secondname'                 => 'Second name',
        'first-lastname'             => 'Surname',
        'second-lastname'            => 'Second surname',
        'user'                       => 'Username',
        'email-standard'             => 'E-mail',
        'password'                   => 'Password',
        'confirm-password'           => 'Confirm Password',
        'status'                     => 'State',
        'save'                       => 'Save',
        'profile'                    => 'Profile',
        'Desarrollado por'           => 'Developed by',
        'para'                       => 'for',
        'enter_the_code'             => 'Enter the code',
        'click_for_restore_password' => 'Click to reset your password',
        'new_password_is'            => 'Your new password is:',
        'loging'                     => 'Log in.',
        'Bienvenido(a)'              => 'Welcome',
        'Versión {ver}'              => 'Version {ver}',
        'V {ver}'                    => 'V {ver}',
        'Home'                       => 'Home',
    ],
    'errors' => [
        'RESTRICTED_AREA'              => 'Try to access a restricted area. For which you do not have permissions.',
        'MISSING_OR_UNEXPECTED_PARAMS' => 'Missing or unexpected parameters.',
    ],
    'users' => [
        'NO_ERROR'                     => '',
        'DUPLICATE_USER'               => 'The user "%1$s" is in use.',
        'DUPLICATE_EMAIL'              => 'The email "%1$s" is in use.',
        'CODE_DUPLICATE'               => 'The code "%1$s" is in use.',
        'INCORRECT_PASSWORD'           => 'Incorrect password.',
        'USER_NO_EXISTS'               => 'The user "%1$s" does not exist.',
        'UNEXPECTED_ACTION'            => 'Unexpected action.',
        'ACTIVE_SESSION'               => 'There is already an active session.',
        'BLOCKED_FOR_ATTEMPTS'         => 'User "%1$s" has been blocked because it has reached the limit of failed attempts.',
        'DUPLICATE_DOCUMENT'           => 'The identification document "%1$s" is already registered.',
        'USER_CREATED'                 => 'User Created',
        'USER_EDITED'                  => 'User edited',
        'USER_DELETED'                 => 'User Deleted',
        'USER_ERROR_ON_CREATION'       => 'Error creating user, try later.',
        'USER_NOT_BELONG_INSTITUTION'  => 'The user is not associated with the selected institution.',
        'EXPIRED_OR_NOT_EXIST_CODE'    => 'The code has expired or does not exist.',
        'NOT_MATCH_PASSWORDS'          => 'Passwords do not match.',
        'RESTRICTED_AREA'              => 'Try to access a restricted area. For which you do not have permissions.',
        'MISSING_OR_UNEXPECTED_PARAMS' => 'Missing or unexpected parameters.',
        'INACTIVE_USER'                => 'The user "%1$s" is deactivated.',
        'ORGANIZATION_IS_NOT_ACTIVE'   => 'The organization "%1$s" is not actived.',
        'INVALID_TWO_FACTOR_CODE'      => 'Invalid two-factor authentication code.',
    ],
    'adminZone' => [
        'Crear ticket de soporte'                                    => 'Create support ticket',
        'Ticket de soporte creado.'                                  => 'Support ticket created.',
        'Ha ocurrido un error inesperado.'                           => 'An unexpected error has occurred.',
        'Información faltante o inesperada.'                         => 'Missing or unexpected information.',
        'Ticket soporte dentro del panel administrativo (osTicket).' => 'Ticket support within the administrative panel (osTicket).',
        'Datos de perfil'                                            => 'Profile data',
        'Imagen de perfil'                                           => 'Profile image',
        'Intentos de ingresos'                                       => 'Logins attempts',
        'Usuario sin ingresos'                                       => 'User without login',
        'Registro de ingresos'                                       => 'Record of login',
        'Agregar usuario'                                            => 'Add user',
        'Gestionar usuarios'                                         => 'Users management',
        'Log de errores'                                             => 'Errors log',
        'Rutas y permisos'                                           => 'Routes and permissions',
        'Importar usuarios'                                          => 'Users import',
        'Acerca de'                                                  => 'About',
        'Editar foto'                                                => 'Edit profile picture',
        'Cuenta'                                                     => 'Account',
        'Contraseña'                                                 => 'Password',
        'Complementaria'                                             => 'Complementary',
        'Información de perfil'                                      => 'Profile information',
        'Nombres'                                                    => 'Names',
        'Primer nombre'                                              => 'First name',
        'Segundo nombre'                                             => 'Second name',
        'Apellidos'                                                  => 'Surnames',
        'Primer apellido'                                            => 'First surname',
        'Segundo apellido'                                           => 'Last surname',
        'Usuario'                                                    => 'User',
        'Correo'                                                     => 'Email',
        'Guardar'                                                    => 'Save',
        'Cambiar de contraseña'                                      => 'Change password',
        'Contraseña actual'                                          => 'Current password',
        'Nueva contraseña'                                           => 'New password',
        'Confirmar contraseña'                                       => 'Confirm password',
        'Cancelar'                                                   => 'Cancel',
        'Perfil'                                                     => 'Profile',
        'Editar Cuenta'                                              => 'Edit account',
        'Cambiar contraseña'                                         => 'Change password',
        'Noticias'                                                   => 'News',
        'Administrativo'                                             => 'Administrative',
    ],
    'avatarModule' => [
        'Ha ocurrido un error desconocido, intente más tarde.' => 'An unknown error has occurred, try later.',
        'MISSING_OR_UNEXPECTED_PARAMS'                         => 'Missing or unexpected parameters.',
        'Imagen de perfil modificada'                          => 'Modified profile image',
    ],
    'sidebarAdminZone' => [
        'Inicio'          => 'Home',
        'Categorías'      => 'Categories',
        'Ubicaciones'     => 'Locations',
        'Soporte técnico' => 'Technical support',
        'Mensajes'        => 'Messages',
        'Cerrar sesión'   => 'Logout',
        'Blog'            => 'Blog',
        'Idiomas'         => 'Languages',
    ],
    'supportFormAdminZone' => [
        'Soporte técnico' => 'Technical support',
        'Asunto'          => 'Subject',
        'Mensaje'         => 'Message',
        'Enviar'          => 'Send',
    ],
    'messenger' => [
        'Enviar nuevo mensaje'                                 => 'Send new message',
        'Redactar mensaje'                                     => 'Compose message',
        'Asunto'                                               => 'Affair',
        'Mensaje'                                              => 'Message',
        'Enviar'                                               => 'Send',
        'Para'                                                 => 'For',
        'Cargar más'                                           => 'Load more',
        'Seleccionar destinatario'                             => 'Select recipient',
        'Cerrar'                                               => 'Close',
        'Escriba su respuesta'                                 => 'Write your answer',
        'No ha podido enviarse el mensaje, intente más tarde.' => 'The message could not be sent, try later.',
        'El mensaje ha sido enviado.'                          => 'The message has been sent.',
        'Los parámetros recibidos no son correctos.'           => 'The parameters received are not correct.',
        'El mensaje ha sido marcado como leído.'               => 'The message has been marked as read.',
        'Ha ocurrido un error, intente luego.'                 => 'An error has occurred, try again later.',
        'El mensaje que intenta modificar no existe.'          => 'The message you are trying to modify does not exist.',
        'Parámetros incompatibles.'                            => 'Incompatible parameters',
        'El usuario no existe.'                                => 'Username does not exist.',
    ],
    'cropper' => [
        'Agregar imagen'                             => 'Add image',
        'Seleccionar imagen'                         => 'Select image',
        'Título de la imagen'                        => 'Image title',
        'La imagen se guardará con las dimensiones:' => 'The image will be saved with the dimensions:',
        'El ancho mínimo es'                         => 'The minimum width is',
        'Cancelar'                                   => 'Cancel',
        'Guardar imagen'                             => 'Save image',
        'Girar'                                      => 'Turn',
        'Voltear'                                    => 'Flip',
        'Ajustar'                                    => 'Adjust',
        'Cambiar'                                    => 'Change',
        'Atrás'                                      => 'Back',
        'Izquierda'                                  => 'Left',
        'Derecha'                                    => 'Right',
        'Horizontal'                                 => 'Horizontal',
        'Vertical'                                   => 'Vertical',
        'Arriba'                                     => 'Up',
        'Abajo'                                      => 'Down',
        'Alejar'                                     => 'Zoom out',
        'Acercar'                                    => 'Zoom in',
        'Cargar imagen'                              => 'Load image',
        'Guardar'                                    => 'Save',
        'Recortar'                                   => 'Crop',
    ],
    'page404' => [
        '404 - Página no encontrada'                                     => '404 - Page not found',
        'Algo está mal aquí'                                             => 'Something is wrong here',
        'El enlace al que intenta ingresar ya no existe o fue cambiado.' => 'The link you are trying to enter no longer exists or was changed.',
        'Ir a Inicio'                                                    => 'Go to home',
    ],
    'page403' => [
        '403 - Acceso denegado'                                                => '403 - Access denied',
        'Algo está mal aquí'                                                   => 'Something is wrong here',
        'El enlace al que intenta ingresar no está disponible para su acceso.' => 'The link you are trying to enter is not available for access.',
        'Ir a Inicio'                                                          => 'Go to home',
    ],
    'page503' => [
        '503 - En mantenimiento'                                   => '503 - In maintenance',
        'Mantenimiento'                                            => 'Maintenance',
        'En curso, no es posible usar la plataforma en un tiempo.' => 'In progress, it is not possible to use the platform for some time.',
    ],
    'routesViewAdminZone' => [
        'Rutas y permisos'          => 'Routes and permissions',
        'Nombre'                    => 'Name',
        'Definición'                => 'Definition',
        'Ruta'                      => 'Route',
        'Clase'                     => 'Class',
        'Método'                    => 'Method',
        'Roles con acceso'          => 'Roles with access',
        'No requiere autenticación' => 'No authentication required',
    ],
    'genericTokenModule' => [
        'Comentarios'                                                           => 'Comments',
        'Correo electrónico'                                                    => 'Email',
        'Asunto'                                                                => 'Affair',
        'Comentario'                                                            => 'Commentary',
        'Requerir respuesta'                                                    => 'Require response',
        'Enviar'                                                                => 'Send',
        'Información.'                                                          => 'Information.',
        'El recurso al que intenta acceder ha expirado o ya ha sido utilizado.' => 'The resource you are trying to access has expired or has already been used.',
        'Mensaje'                                                               => 'Message',
        'El recurso no existe o el enlace ha expirado'                          => 'The resource does not exist or the link has expired',
        'Enviar comentario'                                                     => 'Send comment',
        'Enviado.'                                                              => 'Sent.',
        'Ha ocurrido un error inesperado.'                                      => 'An unexpected error has occurred.',
        'El enlace tendrá validez de una hora.'                                 => 'The link will be valid for one hour.',
        'Responder'                                                             => 'Answer',
    ],
    'usersModule' => [
        'Usuarios'                                                                   => 'Users',
        'Agregar'                                                                    => 'Add',
        '#'                                                                          => '#',
        'Nombres'                                                                    => 'Names',
        'Apellidos'                                                                  => 'Surnames',
        'Correo electrónico'                                                         => 'Email',
        'Usuario'                                                                    => 'Username',
        'Activo/Inactivo'                                                            => 'Active/Inactive',
        'Tipo'                                                                       => 'Type',
        'Acciones'                                                                   => 'Actions',
        'firstname'                                                                  => 'First name',
        'secondname'                                                                 => 'Second name',
        'first-lastname'                                                             => 'Surname',
        'second-lastname'                                                            => 'Second surname',
        'user'                                                                       => 'Username',
        'email-standard'                                                             => 'E-mail',
        'password'                                                                   => 'Password',
        'confirm-password'                                                           => 'Confirm password',
        'status'                                                                     => 'Status',
        'save'                                                                       => 'Save',
        'Guardar'                                                                    => 'Save',
        'password_restored'                                                          => 'New password',
        'profile'                                                                    => 'Profile',
        'current-password'                                                           => 'Current password',
        'Datos de usuario'                                                           => 'User data',
        'Avatar'                                                                     => 'Avatar',
        'Avatar actual'                                                              => 'Current avatar',
        'Cambiar'                                                                    => 'Change',
        'Aún no has seleccionado un avatar.'                                         => 'You have not selected an avatar yet.',
        'Color'                                                                      => 'Color',
        'Cabello'                                                                    => 'Hair',
        'Ojos'                                                                       => 'Eyes',
        'Nariz'                                                                      => 'Nose',
        'Boca'                                                                       => 'Mouth',
        'Cuerpo'                                                                     => 'Body',
        'Ropa'                                                                       => 'Clothes',
        'Seleccionar avatar'                                                         => 'Select avatar',
        'active'                                                                     => 'Active',
        'inactive'                                                                   => 'Inactive',
        'Editar'                                                                     => 'Edit',
        'Sí'                                                                         => 'Yes',
        'No'                                                                         => 'No',
        'Ha ocurrido un error desconocido con los valores ingresados.'               => 'An unknown error has occurred with the values entered.',
        'Verificar sesión'                                                           => 'Verify Session',
        'Creación de usuario'                                                        => 'User creation',
        'Usuario creado.'                                                            => 'User created',
        'Ya existe un usuario con ese email.'                                        => 'A user with that email already exists.',
        'Ya existe un usuario con ese nombre de usuario.'                            => 'A user with that username already exists.',
        'Ya existe un usuario con ese email y nombre de usuario.'                    => 'There is already a user with that email and username.',
        'Las contraseñas no coinciden.'                                              => 'Passwords do not match.',
        'Ha ocurrido un error inesperado.'                                           => 'An unexpected error has occurred.',
        'Edición de usuario'                                                         => 'User Edition',
        'Usuario editado.'                                                           => 'User edited.',
        'La contraseña es errónea.'                                                  => 'The password is wrong.',
        'No existe el usuario que intenta modificar.'                                => 'There is no user trying to modify.',
        'Principal'                                                                  => 'Main',
        'Administrativo'                                                             => 'Administator',
        'General'                                                                    => 'General',
        'Avatar o foto de perfil'                                                    => 'Avatar or profile picture',
        'Foto de perfil'                                                             => 'Profile picture',
        'Puede usar un dibujo personalizado un una imagen.'                          => 'You can use a custom drawing on an image.',
        'Aún no ha seleccionado un avatar o una foto de perfil.'                     => 'You have not selected an avatar or profile picture yet.',
        'Guardar foto de perfil'                                                     => 'Save profile picture',
        'Bloqueado por intentos fallidos'                                            => 'Blocked by unsuccessful attempts',
        'El usuario no está habilitado para usar la este método de inicio de sesión' => 'The user is not enabled to use this login method',
        'Agregar usuario'                                                            => 'Add user',
        'Agregar nuevo usuario ${type}'                                              => 'Add new ${type} user',
    ],
    'loginReport' => [
        'Registro de ingresos'           => 'Login record',
        'Usuarios que han ingresado'     => 'Users who have entered',
        'Usuarios que no han ingresado'  => 'Users who have not logged in',
        'Registro de intentos de inicio' => 'Attempts log',
        'Nombre'                         => 'Name',
        'Último acceso'                  => 'Last access',
        'Tiempo en plataforma'           => 'Time on platform',
        'ID'                             => 'ID',
        'Usuario ingresado'              => 'User Login',
        'Intento exitoso'                => 'Successful attempt',
        'Información'                    => 'information',
        'IP'                             => 'IP',
        'Fecha'                          => 'Date',
    ],
    'mailTemplates' => [
        'Mensaje'                              => 'Message',
        'Asunto'                               => 'Subject',
        'E-mail'                               => 'E-mail',
        'Nombre'                               => 'Name',
        'Extra'                                => 'Extra',
        'Recuperación de contraseña'           => 'Password recovery',
        'Ingresar el código'                   => 'Enter the code',
        'MENSAJE_DE_VALIDEZ'                   => 'Remember that this code is valid for 24 hours, after that time you must generate a new one',
        'Click para restablecer su contraseña' => 'Click to reset your password',
        'Iniciar sesión.'                      => 'Log in.',
        'Su nueva contraseña es'               => 'Your new password is',
        'Código de verificación'               => 'Verification code',
        'Enviado desde'                        => 'Sent from',
    ],
    'usersProblems' => [
        'Creación de solicitud de soporte'                                => "Creation of the request for support",
        'Volver atrás'                                                    => "Go back",
        'Solicitud de soporte'                                            => "Support Request",
        'Su solicitud de soporte <br> ha sido creada'                     => "Your support request <br> has been created",
        'Enviar'                                                          => "Send",
        'Ingresar'                                                        => "Log in",
        'Paso'                                                            => "Step",
        'Correo no registrado'                                            => "Unregistered e-mail",
        'Código incorrecto'                                               => "Incorrect code",
        'Su contraseña ha sido cambiada'                                  => "Your password has been changed",
        'Siguiente'                                                       => "Next",
        'Ya tengo un código'                                              => "I already have a code",
        'Introducir un email diferente'                                   => "Enter a different email",
        'Restablecer contraseña'                                          => "Restore password",
        'Digite su correo electrónico'                                    => "Enter your email",
        'Ingrese su nueva contraseña'                                     => "Enter your new password",
        'Confirme su nueva contraseña'                                    => "Confirm your new password",
        'Solución a problemas de ingreso'                                 => "Solution to login problems",
        'No recuerdo mi usuario'                                          => "I do not remember my user",
        'No recuerdo mi contraseña'                                       => "I do not remember my password",
        'No recuerda la contraseña asignada'                              => "Do not remember the assigned password",
        'Usuario bloqueado'                                               => "User is locked",
        'Nombres'                                                         => "Names",
        'Apellidos'                                                       => "Surnames",
        'Correo electrónico'                                              => "Email",
        'Problema presentado'                                             => "Problem presented",
        'Desbloquear mi usuario'                                          => "Unlock my user",
        'Usuario desbloqueado'                                            => "User Unlocked",
        'Ingrese su correo electrónico'                                   => "Enter your email",
        'Introducir un correo diferente'                                  => "Enter a different email",
        'No recuerdo <br> mi usuario'                                     => "I don't <br> remember my user",
        'Usuario recuperado'                                              => "User Recovered",
        'Se ha enviado un mensaje al correo proporcionado.'               => 'A message has been sent to the email provided.',
        'Solicitud por nombre de usuario olvidado.'                       => 'Request for forgotten username.',
        'Solicitud de desbloqueo de usuario.'                             => 'User unlock request.',
        'Su nombre de usuario es'                                         => 'Your username is',
        'Su usuario ha sido desbloqueado.'                                => 'Your user has been unlocked.',
        'No se ha podido procesar la información, intente más tarde.'     => 'The information could not be processed, try again later.',
        'El usuario no ha podido desbloquearse, contacte con el soporte.' => 'The user could not be unlocked, contact support.',
        'El usuario no está bloqueado.'                                   => 'The user is not locked.',
        'Otros inconvenientes (osTicket).'                                => 'Other inconveniences (osTicket).',
        'No se ha podido enviar el mensaje, intente más tarde.'           => 'The message could not be sent, try again later.',
        'Código de verificación'                                          => 'Verification code',
        'Ticket genérico'                                                 => 'Generic ticket',
        'Otro problema'                                                   => 'Another problem',
        'Ayuda para ingresar'                                             => 'Login help',
        'Iniciar sesión'                                                  => 'Login',
        'Usuario'                                                         => 'Username',
        'No recuerdo'                                                     => 'I forgot my',
        'Contraseña'                                                      => 'Password',
        'Atrás'                                                           => 'Back',
    ],
    'userLogin' => [
        'Intentar nuevamente'           => 'Try again',
        '¿Problemas para ingresar?'     => 'Problems to log in?',
        'Digita tu nombre de usuario'   => 'Enter your username',
        'Digita tu contraseña'          => 'Enter your password',
        'Ingresar'                      => 'Log in',
        'Usuario'                       => 'Username',
        'Contraseña'                    => 'Password',
        'NEED_HELP_TO_LOGIN'            => 'Need ${ot}help logging in?${ct}',
        'correo@dominio.com'            => 'name@domain.com',
        'Error al ingresar'             => 'Error when logging in',
        'Todos los derechos reservados' => 'All rights reserved',
        'Recordarme'                    => 'Remember me',
        'WELCOME_MSG'                   => '<h1>Hello!</h1><span>Welcome</span>',
    ],
    'locationBackend' => [
        'Agregar'                                                   => 'Add',
        'Editar'                                                    => 'Edit',
        'País'                                                      => 'Country',
        'Países'                                                    => 'Countries',
        'Departamento'                                              => 'Department',
        'Nombre'                                                    => 'Name',
        'Activo/Inactivo'                                           => 'Active/Inactive',
        'Guardar'                                                   => 'Save',
        'ID'                                                        => 'ID',
        'Acciones'                                                  => 'Actions',
        'Ciudad'                                                    => 'City',
        'Dirección'                                                 => 'Address',
        '(localidad, caserío, barrio, etc...)'                      => '(locality, hamlet, neighborhood, etc ...)',
        'Ubicación en el mapa de la localidad'                      => 'Locality on the map',
        'Vista satelital'                                           => 'Satellite view',
        'Vista de dibujo'                                           => 'Drawing view',
        'Centrar'                                                   => 'Center',
        '(para mayor precisión, puede mover el cursor de posición)' => '(for greater accuracy, you can move the position cursor)',
        'Coordenadas'                                               => 'Coordinates',
        'Departamentos'                                             => 'Departments',
        'Listado de los países'                                     => 'List of countries',
        'Listado de los departamentos'                              => 'List of departments',
        'Ciudades'                                                  => 'Cities',
        'Listado de las ciudades'                                   => 'List of cities',
        'Localidades'                                               => 'Localities',
        'Listado de las localidades'                                => 'List of localities',
        'Ver'                                                       => 'Watch',
        'Ubicaciones'                                               => 'Locations',
        'Localidad'                                                 => 'Locality',
        'Activa'                                                    => 'Active',
        'Inactiva'                                                  => 'Inactive',
        'Activo'                                                    => 'Active',
        'Inactivo'                                                  => 'Inactive',
        'Modificar ciudad'                                          => 'Modify city',
        'Crear ciudad'                                              => 'Create city',
        'Los parámetros recibidos son erróneos.'                    => 'The parameters received are wrong.',
        'La ciudad que intenta modificar no existe'                 => 'The city you are trying to modify does not exist',
        'Ciudad creada.'                                            => 'City created',
        'Datos guardados.'                                          => 'Saved data',
        'Ha ocurrido un error desconocido.'                         => 'An unknown error has occurred.',
        'Ya existe una ciudad con ese nombre.'                      => 'There is already a city with that name.',
        'Sin acciones'                                              => 'No actions',
        'Modificar localidad'                                       => 'Edit locality',
        'Crear localidad'                                           => 'Create locality',
        'La localidad que intenta modificar no existe'              => 'The locality you are trying to modify does not exist',
        'Localidad creada.'                                         => 'Locality created.',
        'Ya existe una localidad con ese nombre.'                   => 'There is already a locality with that name.',
        'Modificar departamento'                                    => 'Edit department',
        'Crear departamento'                                        => 'Create department',
        'El departamento que intenta modificar no existe'           => 'The department you are trying to modify does not exist',
        'Departamento creado.'                                      => 'Department created.',
        'Ya existe un departamento con ese nombre.'                 => 'There is already an apartment with that name.',
        'Crear país'                                                => 'Create country',
        'Modificar país'                                            => 'Modify country',
        'El país que intenta modificar no existe'                   => 'The country you are trying to modify does not exist',
        'País creado.'                                              => 'Country created',
        'Ya existe un país con ese nombre.'                         => 'There is already a country with that name.',
        'Código'                                                    => 'Code',
        'Ya existe un país con ese código.'                         => 'There is already a country with that code.',
        'Ya existe un departamento con ese código.'                 => 'There is already a department with that code.',
        'Ya existe un municipio con ese código.'                    => 'There is already a municipality with that code.',
    ],
    'revoveryPasswordModule' => [
        'Se ha enviado un mensaje al correo proporcionado.' => 'A message has been sent to the email provided.',
        'Solicitud de restablecimiento de contraseña.'      => 'Password reset request.',
        'El recurso solicitado no existe.'                  => 'The requested resource does not exist.',
        'Contraseña cambiada.'                              => 'Password changed.',
        'Recuperación de contraseña'                        => 'Password recovery',
        'Contraseña nueva'                                  => 'New password',
    ],
    'datatables' => [
        'Buscar'              => 'To look for...',
        'Buscador'            => 'Search',
        'Resultados visibles' => 'Visible results',
        'Ordenar por'         => 'Sort by',
        'ASC'                 => '<i class="ui icon arrow down"></i>Ascendent',
        'DESC'                => '<i class="ui icon arrow up"></i>Descendent',
        "Ver"                 => "See",
        "elementos"           => "elements",
    ],
    'bi-dynamic-images' => [
        'Imágenes principales' => 'Main images',
        'Imágenes'             => 'Images',
        'Regresar'             => 'To return',
        'Ver'                  => 'See',
        'Opciones'             => 'Options',
    ],
];

return $lang;
