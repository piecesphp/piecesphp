<?php

/**
 * RouteGroup.php
 */
namespace PiecesPHP\Core;

use PiecesPHP\Core\Routing\RouteGroupAdapter;

/**
 * RouteGroup
 *
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class RouteGroup extends RouteGroupAdapter
{}
