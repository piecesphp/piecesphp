<?php

/**
 * Route.php
 */
namespace PiecesPHP\Core;

use PiecesPHP\Core\Routing\RouteAdapter;

/**
 * Route
 *
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class Route extends RouteAdapter
{}
