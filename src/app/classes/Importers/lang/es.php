<?php

return [
    'TEXTO_IMPORTACION_USUARIOS'             => 'Con este importador podrá realizar la carga masiva de usuarios; debe tener presente que para realizar la carga el archivo de Excel debe contener una información mínima, la cual será verificada registro por registro y en caso de no cumplir uno de los parámetros obligatorios el usuario no será creado en sistema.',
    'TEXTO_IMPORTACION_PERSONAS_HABILITADAS' => 'Con este importador podrá realizar la carga masiva de personas habilitadas; debe tener presente que para realizar la carga el archivo de Excel debe contener una información mínima, la cual será verificada registro por registro y en caso de no cumplir uno de los parámetros obligatorios el registro no será creado en sistema.',
];
