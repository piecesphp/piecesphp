<?php

return [
    "Publicaciones"                                                        => "Publications",
    "Categorías"                                                           => "Categories",
    "Categoría"                                                            => "Category",
    "La categoría que intenta modificar no existe."                        => "The category you are trying to modify does not exist.",
    "Categoría creada."                                                    => "Category created.",
    "Datos guardados."                                                     => "Data saved.",
    "Ha ocurrido un error desconocido."                                    => "An unknown error has occurred.",
    "Ha ocurrido un error desconocido al procesar los valores ingresados." => "An unknown error occurred while processing the entered values.",
    "El idioma \"%s\" no está permitido."                                  => "The language \"%s\" is not allowed.",
    "Eliminar categoría"                                                   => "Delete category",
    "La categoría que intenta eliminar no existe."                         => "The category you are trying to delete does not exist.",
    "Categoría eliminada."                                                 => "Category deleted.",
    "No está permitido eliminar esta categoría."                           => "It is not allowed to delete this category.",
    "Publicación"                                                          => "Publication",
    "La publicación que intenta modificar no existe."                      => "The publication you are trying to modify does not exist.",
    "Publicación creada."                                                  => "Publication created.",
    "No hay existe la categoría."                                          => "The category does not exist.",
    "Eliminar publicación"                                                 => "Delete publication",
    "La publicación que intenta eliminar no existe."                       => "The publication you are trying to delete does not exist.",
    "Publicación eliminada."                                               => "Publication deleted.",
    "Editar"                                                               => "Edit",
    "Eliminar"                                                             => "Delete",
    "Ya existe la categoría."                                              => "Category already exists.",
    "Ya existe la publicación."                                            => "The publication already exists.",
    "Regresar"                                                             => "Back",
    "Agregar categoría"                                                    => "Add category",
    "Nombre"                                                               => "Name",
    "Agregar"                                                              => "Add",
    "Guardar"                                                              => "Save",
    "Idiomas"                                                              => "Languages",
    "Agregar publicación"                                                  => "Add publication",
    "#"                                                                    => "#",
    "Título"                                                               => "Title",
    "Visitas"                                                              => "Visits",
    "Fecha inicial"                                                        => "Start date",
    "Fecha final"                                                          => "End date",
    "Creación"                                                             => "Creation",
    "Edición"                                                              => "Edition",
    "Autor"                                                                => "Author",
    "Acciones"                                                             => "Actions",
    "Datos básicos"                                                        => "Basic data",
    "Imágenes"                                                             => "Images",
    "Detalles"                                                             => "Details",
    "SEO"                                                                  => "SEO",
    "Contenido"                                                            => "Content",
    "Imagen principal"                                                     => "Main image",
    "Imagen miniatura"                                                     => "Thumbnail image",
    "Iniciar"                                                              => "Start",
    "Finalizar"                                                            => "End",
    "Imagen"                                                               => "Image",
    "Descripción"                                                          => "Description",
    "Cargar más"                                                           => "Load more",
    "Anexos"                                                               => "Attachments",
    "Anexo"                                                                => "Attachment",
    "Anexo #"                                                              => "Attachment #",
    "Copia en PDF"                                                         => "PDF Copy",
    "TODAS_LAS_PUBLICACIONES"                                              => "All",
    "PUBLICACIONES_PUBLICADAS"                                             => "Published",
    "PUBLICACIONES_PROGRAMADAS"                                            => "Scheduled",
    "PUBLICACIONES_BORRADOR"                                               => "Draft",
    "Listado de publicaciones"                                             => "List of publications",
    "Listado de categorías"                                                => "List of categories",
    "Inicio"                                                               => "Home",
    "Categorías de publicaciones"                                          => "Categories of publications",
    "Categoría de publicación"                                             => "Category of publications",
    "Publicado"                                                            => "Published",
    "Borrador"                                                             => "Draft",
    "Programado"                                                           => "Scheduled",
    "Publicado"                                                            => "Published",
    "Sí"                                                                   => "Yes",
    "No"                                                                   => "No",
    "Ver"                                                                  => "See",
    "Edición de publicación"                                               => "Publication edition",
    "Agregar publicación"                                                  => "Add publication",
    "Edición de categoría"                                                 => "Category edition",
    "Agregar categoría"                                                    => "Add category",
    "Destacado"                                                            => "Featured",
    "publicaciones"                                                        => "publications",
    "publicación"                                                          => "publication",
];
