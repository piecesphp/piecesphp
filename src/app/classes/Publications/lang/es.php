<?php

return [
    "TODAS_LAS_PUBLICACIONES"   => "Todas",
    "PUBLICACIONES_PUBLICADAS"  => "Publicadas",
    "PUBLICACIONES_PROGRAMADAS" => "Programadas",
    "PUBLICACIONES_BORRADOR"    => "Borradores",
    "Status"                    => "Estado",
];
