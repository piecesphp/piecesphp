<?php

/**
 * OrganizationMapper.php
 */

namespace Organizations\Mappers;

use App\Locations\Mappers\CityMapper;
use App\Locations\Mappers\StateMapper;
use App\Model\UsersModel;
use Organizations\Exceptions\DuplicateException;
use Organizations\OrganizationsLang;
use PiecesPHP\Core\BaseHashEncryption;
use PiecesPHP\Core\Config;
use PiecesPHP\Core\Database\ActiveRecordModel;
use PiecesPHP\Core\Database\EntityMapperExtensible;
use PiecesPHP\Core\Database\Meta\MetaProperty;
use PiecesPHP\Core\StringManipulate;
use PiecesPHP\Core\Validation\Validator;

/**
 * OrganizationMapper.
 *
 * @package     Organizations\Mappers
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2024
 * @property int|null $id
 * @property string|null $preferSlug Es un token usado para acceso individual sin exponer el ID
 * @property string $name
 * @property string $nit
 * @property string|null $size
 * @property string[]|null $actionLines
 * @property string|null $esal
 * @property int|StateMapper|null $state
 * @property int|CityMapper|null $city
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $informativeEmail
 * @property string|null $billingEmail
 * @property string|null $contactName
 * @property string|null $contactPhone
 * @property string|null $contactEmail
 * @property string|null $logo
 * @property string|null $rut
 * @property string $folder
 * @property string|\DateTime $createdAt
 * @property string|\DateTime $updatedAt
 * @property int|UsersModel $createdBy
 * @property int|UsersModel|null $modifiedBy
 * @property int $status
 * @property \stdClass|string|null $meta
 * @property \stdClass|null $langData
 */
class OrganizationMapper extends EntityMapperExtensible
{

    protected $fields = [
        'id' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'preferSlug' => [
            'type' => 'text',
            'null' => true,
        ],
        'name' => [
            'type' => 'text',
        ],
        'nit' => [
            'type' => 'text',
        ],
        'size' => [
            'type' => 'text',
            'null' => true,
        ],
        'actionLines' => [
            'type' => 'json',
            'null' => true,
        ],
        'esal' => [
            'type' => 'text',
            'null' => true,
        ],
        'state' => [
            'type' => 'int',
            'reference_table' => StateMapper::PREFIX_TABLE . StateMapper::TABLE,
            'reference_field' => 'id',
            'reference_primary_key' => 'id',
            'human_readable_reference_field' => 'id',
            'mapper' => StateMapper::class,
            'null' => true,
        ],
        'city' => [
            'type' => 'int',
            'reference_table' => CityMapper::PREFIX_TABLE . CityMapper::TABLE,
            'reference_field' => 'id',
            'reference_primary_key' => 'id',
            'human_readable_reference_field' => 'id',
            'mapper' => CityMapper::class,
            'null' => true,
        ],
        'address' => [
            'type' => 'text',
            'null' => true,
        ],
        'phone' => [
            'type' => 'text',
            'null' => true,
        ],
        'informativeEmail' => [
            'type' => 'text',
            'null' => true,
        ],
        'billingEmail' => [
            'type' => 'text',
            'null' => true,
        ],
        'contactName' => [
            'type' => 'text',
            'null' => true,
        ],
        'contactPhone' => [
            'type' => 'text',
            'null' => true,
        ],
        'contactEmail' => [
            'type' => 'text',
            'null' => true,
        ],
        'logo' => [
            'type' => 'text',
            'null' => true,
        ],
        'rut' => [
            'type' => 'text',
            'null' => true,
        ],
        'folder' => [
            'type' => 'text',
        ],
        'createdAt' => [
            'type' => 'datetime',
            'default' => 'timestamp',
        ],
        'updatedAt' => [
            'type' => 'datetime',
            'null' => true,
        ],
        'createdBy' => [
            'type' => 'int',
            'reference_table' => UsersModel::TABLE,
            'reference_field' => 'id',
            'reference_primary_key' => 'id',
            'human_readable_reference_field' => 'username',
            'mapper' => UsersModel::class,
        ],
        'modifiedBy' => [
            'type' => 'int',
            'reference_table' => UsersModel::TABLE,
            'reference_field' => 'id',
            'reference_primary_key' => 'id',
            'human_readable_reference_field' => 'username',
            'mapper' => UsersModel::class,
            'null' => true,
        ],
        'status' => [
            'type' => 'int',
            'default' => self::ACTIVE,
        ],
        'meta' => [
            'type' => 'json',
            'null' => true,
            'dafault' => null,
        ],
    ];

    const ACTIVE = 1;
    const INACTIVE = 2;
    const DELETED = 0;
    const STATUSES = [
        self::ACTIVE => 'Activa',
        self::INACTIVE => 'Inactiva',
        self::DELETED => 'Eliminada',
    ];

    const SIZE_SMALL = "SMALL";
    const SIZE_MEDIUM = "MEDIUM";
    const SIZE_BIG = "BIG";
    const SIZES = [
        self::SIZE_SMALL => 'Pequeña',
        self::SIZE_MEDIUM => 'Mediana',
        self::SIZE_BIG => 'Grande',
    ];

    const ESAL_YES = "YES";
    const ESAL_NO = "NO";
    const ESAL_OPTIONS = [
        self::ESAL_YES => 'Sí',
        self::ESAL_NO => 'No',
    ];

    const ACTION_LINE_1 = "1";
    const ACTION_LINE_2 = "2";
    const ACTION_LINE_3 = "3";
    const ACTION_LINES = [
        self::ACTION_LINE_1 => 'Línea #1',
        self::ACTION_LINE_2 => 'Línea #2',
        self::ACTION_LINE_3 => 'Línea #3',
    ];

    const INITIAL_ID_GLOBAL = -10;

    const CAN_DELETE_ALL = [
        UsersModel::TYPE_USER_ROOT,
    ];
    const CAN_ASSIGN_ALL = [
        UsersModel::TYPE_USER_ROOT,
    ];
    const EDITORS = [
        UsersModel::TYPE_USER_ADMIN,
        UsersModel::TYPE_USER_GENERAL,
    ];

    const TABLE = 'organizations_elements';
    const VIEW_ACTIVE_DATE = 'organizations_active_date_elements';
    const LANG_GROUP = OrganizationsLang::LANG_GROUP;
    const ORDER_BY_PREFERENCE = [
        '`name` ASC',
        '`nit` ASC',
    ];

    /**
     * Propiedades que no necesitan multi-idioma
     * Si está vacía se llenará automáticamente con los campos no incluidos en $translatableProperties
     * @var string[]
     */
    protected $noTranslatableProperties = [];

    /**
     * Propiedades necesitan multi-idioma
     *
     * @var string[]
     */
    protected $translatableProperties = [];

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @param int $value
     * @param string $fieldCompare
     * @return static
     */
    public function __construct(int $value = null, string $fieldCompare = 'primary_key')
    {

        $this->addMetaProperty(new MetaProperty(MetaProperty::TYPE_JSON, new \stdClass, true), 'langData');
        parent::__construct($value, $fieldCompare);

        //Definición de campos no traducibles en caso de que estén vacíos
        $fields = array_keys($this->fields);
        if (count($this->noTranslatableProperties) == 0) {
            foreach ($fields as $fieldName) {
                if (!in_array($fieldName, $this->translatableProperties) && $this->metaColumnName !== $fieldName) {
                    $this->noTranslatableProperties[] = $fieldName;
                }
            }
        }
    }

    /**
     * @return string
     */
    public function createdByFullName()
    {
        $createdBy = $this->createdBy;

        if (!is_object($createdBy)) {
            $this->createdBy = new UsersModel($createdBy);
            $createdBy = $this->createdBy;
        }

        return $createdBy->getFullName();
    }

    /**
     * @return string|null
     */
    public function modifiedByFullName()
    {
        $modifiedBy = $this->modifiedBy;

        if (!is_object($modifiedBy) && $modifiedBy !== null) {
            $this->modifiedBy = new UsersModel($modifiedBy);
            $modifiedBy = $this->modifiedBy;
        }

        return $modifiedBy !== null ? $modifiedBy->getFullName() : null;
    }

    /**
     * @param string $format
     * @param array $replaceTemplate Para remplazar contenido dentro del formato, el array debe ser ['VALOR_A_REEMPLAZAR' => 'VALOR_DE_REEMPLAZO']
     * @return string
     */
    public function createdAtFormat(string $format = null, array $replaceTemplate = [])
    {
        $format = is_string($format) ? $format : get_default_format_date();
        $formated = localeDateFormat($format, $this->createdAt, $replaceTemplate);
        return $formated;
    }

    /**
     * @param string $format
     * @param array $replaceTemplate Para remplazar contenido dentro del formato, el array debe ser ['VALOR_A_REEMPLAZAR' => 'VALOR_DE_REEMPLAZO']
     * @return string|null
     */
    public function updatedAtFormat(string $format = null, array $replaceTemplate = [])
    {
        $format = is_string($format) ? $format : get_default_format_date();
        $formated = $this->updatedAt instanceof \DateTime  ? localeDateFormat($format, $this->updatedAt, $replaceTemplate) : null;
        return $formated;
    }

    /**
     * Devuelve el slug
     *
     * @param string $lang
     * @return string
     */
    public function getSlug(string $lang = null)
    {
        return self::elementFriendlySlug($this, $lang);
    }

    /**
     * @inheritDoc
     */
    public function save()
    {

        if (self::existsByNit($this->nit, -1)) {
            throw new DuplicateException(__(self::LANG_GROUP, 'Ya existe la organización.'));
        }

        $this->createdAt = new \DateTime();
        $this->createdBy = getLoggedFrameworkUser()->id;
        $saveResult = parent::save();

        if ($saveResult) {
            $idInserted = $this->getInsertIDOnSave();
            if ($idInserted !== null) {
                $this->id = $idInserted;
                $this->preferSlug = self::getEncryptIDForSlug($idInserted);
                $this->update(true);
            }
        }

        return $saveResult;

    }

    /**
     * @param bool $noDateUpdate
     * @inheritDoc
     */
    public function update(bool $noDateUpdate = false)
    {

        if (self::existsByNit($this->nit, $this->id)) {
            throw new DuplicateException(__(self::LANG_GROUP, 'Ya existe la organización.'));
        }
        if (!$noDateUpdate) {
            $this->modifiedBy = getLoggedFrameworkUser()->id;
            $this->updatedAt = new \DateTime();
        }
        return parent::update();
    }

    /**
     * Define una propiedad que está habilitada en multi-idioma en un idioma en específico
     * @param string $lang
     * @param string $property
     * @param mixed $data
     * @return static
     */
    public function setLangData(string $lang, string $property, $data)
    {

        $translatables = $this->translatableProperties;
        $noTranslatables = $this->noTranslatableProperties;

        if (in_array($property, $translatables)) {

            if ($lang !== Config::get_default_lang()) {

                if (!isset($this->langData->$lang)) {
                    $this->langData->$lang = new \stdClass;
                }

                $this->langData->$lang->$property = $data;

            } else {
                $this->$property = $data;
            }

        } elseif (in_array($property, $noTranslatables)) {
            $this->$property = $data;
        }

        return $this;
    }

    /**
     * Obtiene una propiedad que está habilitada en multi-idioma en un idioma en específico o
     * el valor de la propiedad por defecto
     * @param string $lang
     * @param string $property
     * @param bool $defaultOnEmpty
     * @param mixed $returnOnEmpty
     * @return mixed
     * Si la propiedad no existe se devolerá null
     * Si $defaultOnEmpty está en true, cuando no exista en el idioma seleccionado se tomará de las propiedades principales
     * Si $defaultOnEmpty está en false, cuando no exista en el idioma seleccionado se devolverá $returnOnEmpty
     */
    public function getLangData(string $lang, string $property, bool $defaultOnEmpty = true, $returnOnEmpty = '')
    {
        if (isset($this->langData->$lang) && isset($this->langData->$lang->$property)) {

            $value = $this->langData->$lang->$property;

            //Si para una propiedad se quiere un compartamiento particular en su obtención
            //puede definírsele aquí
            $specialBehaviour = [
                'SAMPLE_FIELD' => function ($value) {
                    return $value;
                },
            ];

            return array_key_exists($property, $specialBehaviour) ? ($specialBehaviour[$property])($value) : $value;

        } elseif ($defaultOnEmpty || $lang === Config::get_default_lang()) {

            $propertiesExpected = array_merge($this->noTranslatableProperties, $this->translatableProperties);

            if (in_array($property, $propertiesExpected)) {

                $value = $this->$property;

                //Si para una propiedad se quiere un compartamiento particular en su obtención
                //puede definírsele aquí
                $specialBehaviour = [
                    'SAMPLE_FIELD' => function ($value) {
                        return $value;
                    },
                ];

                return array_key_exists($property, $specialBehaviour) ? ($specialBehaviour[$property])($value) : $value;

            }

            return null;

        } else {

            return $returnOnEmpty;

        }

    }

    /**
     * @param string $property
     * @return mixed
     */
    public function currentLangData(string $property)
    {
        $lang = Config::get_lang();
        return $this->getLangData($lang, $property);
    }

    /**
     * Devuelve los campos que no son traducibles
     *
     * @return string[]
     */
    public function getNoTranslatableProperties()
    {
        return $this->noTranslatableProperties;
    }

    /**
     * Devuelve los campos que son traducibles
     *
     * @return string[]
     */
    public function getTranslatableProperties()
    {
        return $this->translatableProperties;
    }

    /**
     * Campos extra:
     *  - idPadding
     *  - stateName
     *  - cityName
     *  - sizeText
     *  - actionLinesText
     *  - esalText
     *  - statusText
     * @return string[]
     */
    public static function fieldsToSelect(string $formatDate = '%d-%m-%Y')
    {

        $mapper = (new OrganizationMapper);
        $model = $mapper->getModel();
        $table = $model->getTable();

        $tableState = StateMapper::PREFIX_TABLE . StateMapper::TABLE;
        $tableCity = CityMapper::PREFIX_TABLE . CityMapper::TABLE;
        $tableUser = UsersModel::TABLE;

        $defaultLang = Config::get_default_lang();
        $currentLang = Config::get_lang();

        $stateName = "SELECT {$tableState}.name FROM {$tableState} WHERE {$tableState}.id = {$table}.state";
        $cityName = "SELECT {$tableCity}.name FROM {$tableCity} WHERE {$tableCity}.id = {$table}.city";

        $statusesJSON = json_encode((object) self::statuses(), \JSON_UNESCAPED_UNICODE);
        $sizesJSON = json_encode((object) self::sizes(), \JSON_UNESCAPED_UNICODE);
        $actionLinesJSON = json_encode((object) self::actionLines(), \JSON_UNESCAPED_UNICODE);
        $esalOptionsJSON = json_encode((object) self::esalOptions(), \JSON_UNESCAPED_UNICODE);

        $fields = [
            "LPAD({$table}.id, 5, 0) AS idPadding",
            "({$stateName}) AS stateName",
            "({$cityName}) AS cityName",
            "JSON_UNQUOTE(JSON_EXTRACT('{$sizesJSON}', CONCAT('$.', {$table}.size))) AS sizeText",
            "JSON_UNQUOTE(JSON_EXTRACT('{$actionLinesJSON}', CONCAT('$.', {$table}.actionLines))) AS actionLinesText",
            "JSON_UNQUOTE(JSON_EXTRACT('{$esalOptionsJSON}', CONCAT('$.', {$table}.esal))) AS esalText",
            "JSON_UNQUOTE(JSON_EXTRACT('{$statusesJSON}', CONCAT('$.', {$table}.status))) AS statusText",
            "{$table}.meta",
        ];

        if ($defaultLang == $currentLang || !self::jsonExtractExistsMySQL()) {

            //En caso de que las funciones JSON_* no estén disponibles en el motor SQL
            $allFields = array_merge($mapper->getNoTranslatableProperties(), $mapper->getTranslatableProperties());

            foreach ($allFields as $field) {
                $fields[] = "{$table}.{$field}";
            }

        } else {

            $noTranslatables = $mapper->getNoTranslatableProperties();

            foreach ($noTranslatables as $field) {
                $fields[] = "{$table}.{$field}";
            }

            $translatables = $mapper->getTranslatableProperties();

            $specialBehaviour = [
                'FIELD_NAME' => function ($fieldName) {
                    return $fieldName;
                },
            ];

            foreach ($translatables as $fieldToLang) {

                if (array_key_exists($fieldToLang, $specialBehaviour)) {
                    $fields[] = ($specialBehaviour[$fieldToLang])($fieldToLang);
                } else {
                    $normalField = "{$table}.{$fieldToLang}";
                    $langField = "JSON_UNQUOTE(JSON_EXTRACT({$table}.meta, '$.langData.{$currentLang}.{$fieldToLang}'))";
                    $fields[] = "IF({$langField} IS NOT NULL, {$langField}, {$normalField}) AS `{$fieldToLang}`";
                }

            }

        }

        return $fields;

    }

    /**
     * @param string $fieldName
     * @return string
     */
    public static function fieldCurrentLangForSQL(string $fieldName)
    {

        $table = self::TABLE;

        $defaultLang = Config::get_default_lang();
        $currentLang = Config::get_lang();

        $fieldSQL = '';

        if ($defaultLang == $currentLang || !self::jsonExtractExistsMySQL()) {
            $fieldSQL = "{$table}.{$fieldName}";
        } else {
            $jsonExtractField = "JSON_UNQUOTE(JSON_EXTRACT({$table}.meta, '$.langData.{$currentLang}.{$fieldName}'))";
            $fieldSQL = "IF({$jsonExtractField} IS NOT NULL, {$jsonExtractField}, {$table}.{$fieldName})";
        }

        return $fieldSQL;

    }

    /**
     * Configura las versiones de las propiedades según el idioma actual
     *
     * @param \stdClass $element
     * @return \stdClass
     */
    public static function translateEntityObject(\stdClass $element)
    {

        $mapper = self::objectToMapper($element);

        $defaultLang = get_config('default_lang');
        $currentLang = Config::get_lang();

        if ($defaultLang != $currentLang && $mapper !== null) {

            $translatables = $mapper->getTranslatableProperties();

            foreach ($translatables as $property) {
                $element->$property = $mapper->getLangData($currentLang, $property);
            }

        }

        return $element;

    }

    /**
     * Devuelve el nombre amigable del elemento
     *
     * @param \stdClass|OrganizationMapper|int $elementOrID
     * @param string $lang
     * @return string
     */
    public static function elementFriendlySlug($elementOrID, string $lang = null)
    {
        $slug = '';

        if ($elementOrID instanceof \stdClass) {
            if (isset($elementOrID->id) && Validator::isInteger($elementOrID->id)) {
                $elementOrID = (int) $elementOrID->id;
            }
        }

        if (is_int($elementOrID)) {

            $elementOrID = self::getBy($elementOrID, 'id', true);

        }

        if ($elementOrID instanceof OrganizationMapper && $elementOrID->id !== null) {

            $uniqid = $elementOrID->preferSlug !== null ? $elementOrID->preferSlug : self::getEncryptIDForSlug($elementOrID->id);
            $name = StringManipulate::friendlyURLString($lang === null ? $elementOrID->currentLangData('name') : $elementOrID->getLangData($lang, 'name'));

            $slug = "{$name}-{$uniqid}";

        }

        return $slug;
    }

    /**
     * Devuelve el ID desde el Slug válido, de lo contrario devuelve null
     *
     * @param string $slug
     * @return int|null
     */
    public static function extractIDFromSlug(string $slug)
    {
        $slug = explode('-', $slug);
        $slug = is_array($slug) && count($slug) > 1 ? $slug[count($slug) - 1] : null;
        $slug = $slug !== null ? BaseHashEncryption::decrypt(strtr($slug, '._', '-_'), self::TABLE) : null;
        $slug = $slug !== null ? explode('-', $slug) : null;
        $slugID = is_array($slug) && count($slug) === 2 ? $slug[0] : null;
        $slugID = Validator::isInteger($slugID) ? (int) $slugID : null;
        return $slugID;
    }

    /**
     * Devuelve el ID encriptado para generar un Slug
     *
     * @return string
     */
    public static function getEncryptIDForSlug(int $id)
    {
        $uniqid = mb_strtolower(str_replace(['.', '-'], '', uniqid()));
        $uniqid = strtr(BaseHashEncryption::encrypt("{$id}-{$uniqid}", self::TABLE), '-_', '._');
        return $uniqid;
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        $options = [];
        foreach (self::STATUSES as $value => $text) {
            $options[$value] = __(self::LANG_GROUP, $text);
        }
        return $options;
    }

    /**
     * @return array
     */
    public static function sizes()
    {
        $options = [];
        foreach (self::SIZES as $value => $text) {
            $options[$value] = __(self::LANG_GROUP, $text);
        }
        return $options;
    }

    /**
     * @return array
     */
    public static function actionLines()
    {
        $options = [];
        foreach (self::ACTION_LINES as $value => $text) {
            $options[$value] = __(self::LANG_GROUP, $text);
        }
        return $options;
    }

    /**
     * @return array
     */
    public static function esalOptions()
    {
        $options = [];
        foreach (self::ESAL_OPTIONS as $value => $text) {
            $options[$value] = __(self::LANG_GROUP, $text);
        }
        return $options;
    }

    /**
     * Un array listo para ser usado en array_to_html_options
     * @param string $defaultLabel
     * @param string $defaultValue
     * @return array
     */
    public static function statusesForSelect(string $defaultLabel = '', string $defaultValue = '')
    {
        $sourceOptions = self::statuses();
        $defaultLabel = strlen($defaultLabel) > 0 ? $defaultLabel : __(self::LANG_GROUP, 'Estados');
        $options = [];
        $options[$defaultValue] = $defaultLabel;
        foreach ($sourceOptions as $value => $text) {
            if ($value == self::DELETED) {
                continue;
            }
            $options[$value] = $text;
        }
        return $options;
    }

    /**
     * Un array listo para ser usado en array_to_html_options
     * @param string $defaultLabel
     * @param string $defaultValue
     * @return array
     */
    public static function sizesForSelect(string $defaultLabel = '', string $defaultValue = '')
    {
        $sourceOptions = self::sizes();
        $defaultLabel = strlen($defaultLabel) > 0 ? $defaultLabel : __(self::LANG_GROUP, 'Seleccione una opción');
        $options = [];
        $options[$defaultValue] = $defaultLabel;
        foreach ($sourceOptions as $value => $text) {
            $options[$value] = $text;
        }
        return $options;
    }

    /**
     * Un array listo para ser usado en array_to_html_options
     * @param string $defaultLabel
     * @param string $defaultValue
     * @param array $checkAdditionsFromData Verifica si hay información personalizada y la añade
     * @return array
     */
    public static function actionLinesForSelect(string $defaultLabel = '', string $defaultValue = '', array $checkAdditionsFromData = [])
    {
        $sourceOptions = self::actionLines();
        $defaultLabel = strlen($defaultLabel) > 0 ? $defaultLabel : __(self::LANG_GROUP, 'Seleccione una opción');
        $options = [];
        $options[$defaultValue] = $defaultLabel;
        foreach ($sourceOptions as $value => $text) {
            $options[$value] = $text;
        }
        foreach ($checkAdditionsFromData as $i => $additionalValue) {
            if (is_scalar($additionalValue)) {
                $additionalValueIntegerVersion = Validator::isInteger($additionalValue) ? (int) $additionalValue : null;
                $hasAdditionalValueIntegerVersion = $additionalValueIntegerVersion !== null ? array_key_exists($additionalValueIntegerVersion, $sourceOptions) : false;
                if (!array_key_exists($additionalValue, $sourceOptions) && !$hasAdditionalValueIntegerVersion) {
                    $options[$additionalValue] = $additionalValue;
                }
            }
        }
        return $options;
    }

    /**
     * Un array listo para ser usado en array_to_html_options
     * @param string $defaultLabel
     * @param string $defaultValue
     * @return array
     */
    public static function esalOptionsForSelect(string $defaultLabel = '', string $defaultValue = '')
    {
        $sourceOptions = self::esalOptions();
        $defaultLabel = strlen($defaultLabel) > 0 ? $defaultLabel : __(self::LANG_GROUP, 'Seleccione una opción');
        $options = [];
        $options[$defaultValue] = $defaultLabel;
        foreach ($sourceOptions as $value => $text) {
            $options[$value] = $text;
        }
        return $options;
    }

    /**
     * @param bool $asMapper
     * @return static[]|array
     */
    public static function all(bool $asMapper = false, bool $onlyActives = true)
    {
        $model = self::model();

        $selectFields = [];

        $model->select($selectFields);
        if ($onlyActives) {
            $deleted = self::DELETED;
            $model->where("status != {$deleted}");
        }
        $model->execute();

        $result = $model->result();
        $result = is_array($result) ? $result : [];

        if ($asMapper) {
            foreach ($result as $key => $value) {
                $result[$key] = self::objectToMapper($value);
            }
        }

        return $result;
    }

    /**
     * @param string $column
     * @param int $value
     * @param bool $asMapper
     *
     * @return static[]|array
     */
    public static function allBy(string $column, $value, bool $asMapper = false)
    {
        $model = self::model();

        $model->select()->where([
            $column => $value,
        ])->execute();

        $result = $model->result();
        $result = is_array($result) ? $result : [];

        if ($asMapper) {
            foreach ($result as $key => $value) {
                $result[$key] = self::objectToMapper($value);
            }
        }

        return $result;
    }

    /**
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @return static|object|null
     */
    public static function getBy($value, string $column = 'id', bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            $column => $value,
        ];

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        $result = !empty($result) ? $result[0] : null;

        if (!is_null($result) && $as_mapper) {
            $result = self::objectToMapper($result);
        }

        return $result;
    }

    /**
     * @param bool $asMapper
     * @param bool $onlyActives
     * @return \stdClass|static|null
     */
    public static function lastModifiedElement(bool $asMapper = false, bool $onlyActives = false)
    {
        $table = self::TABLE;
        $model = self::model();

        $selectFields = [];

        $model->select($selectFields);

        $model->orderBy("{$table}.updatedAt DESC, {$table}.createdAt DESC");

        $whereString = null;
        $where = [];
        $and = 'AND';

        if ($onlyActives) {

            $statusActive = self::ACTIVE;
            $beforeOperator = !empty($where) ? $and : '';
            $critery = "{$table}.status = {$statusActive}";
            $where[] = "{$beforeOperator} ({$critery})";

        }

        if (!empty($where)) {
            $whereString = implode(' ', $where);
        }

        if ($whereString !== null) {
            $model->where($whereString);
        }

        $model->execute(false, 1, 1);

        $result = $model->result();
        $result = !empty($result) ? $result[0] : null;

        if ($asMapper && $result !== null) {
            $result = self::objectToMapper($result);
        }

        return $result;
    }

    /**
     * @param int $id
     * @return bool
     */
    public static function existsByID(int $id)
    {
        $model = self::model();

        $where = [
            "id = $id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return !empty($result);
    }

    /**
     * Verifica si existe algún registro igual
     *
     * @param string $nit
     * @param int $ignoreID
     * @param bool $onlyNoDeleted
     * @return bool
     */
    public static function existsByNit(string $nit, int $ignoreID = null, bool $onlyNoDeleted = true)
    {

        $ignoreID = $ignoreID !== null ? $ignoreID : -1;
        $model = self::model();

        $nit = escapeString($nit);
        $statusDeleted = self::DELETED;

        $where = [
            "nit = '{$nit}' AND",
            "id != {$ignoreID}",
        ];

        if ($onlyNoDeleted) {
            $where[] = "AND status != {$statusDeleted}";
        }

        $model->select()->where(implode(' ', $where));

        $model->execute();

        $result = $model->result();

        return !empty($result);

    }

    /**
     * @param integer $userType
     * @return bool
     */
    public static function canAssignAnyOrganization(int $userType)
    {
        return in_array($userType, self::CAN_ASSIGN_ALL);
    }

    /**
     * Devuelve el mapeador desde un objeto
     *
     * @param \stdClass $element
     * @return OrganizationMapper|null
     */
    public static function objectToMapper(\stdClass $element)
    {

        $element = (array) $element;
        $mapper = new OrganizationMapper;
        $fieldsFilleds = [];
        $fields = array_merge(array_keys($mapper->fields), array_keys($mapper->getMetaProperties()));

        $defaultPropertiesValues = [];

        foreach ($defaultPropertiesValues as $defaultProperty => $defaultPropertyValue) {
            if (!array_key_exists($defaultProperty, $element)) {
                $element[$defaultProperty] = $defaultPropertyValue;
            }
        }

        $defaultMetaPropertiesValues = [];

        foreach ($element as $property => $value) {

            if (in_array($property, $fields)) {

                if ($property == 'meta') {

                    $value = $value instanceof \stdClass  ? $value : @json_decode($value);

                    foreach ($defaultMetaPropertiesValues as $defaultMetaProperty => $defaultMetaPropertyValue) {
                        foreach ($defaultMetaPropertiesValues as $defaultMetaProperty => $defaultMetaPropertyValue) {
                            if (!property_exists($value, $defaultMetaProperty)) {
                                $value->$defaultMetaProperty = $defaultMetaPropertyValue;
                            }
                        }
                    }

                    if ($value instanceof \stdClass) {
                        foreach ($value as $metaPropertyName => $metaPropertyValue) {

                            if ($mapper->hasMetaProperty($metaPropertyName)) {
                                $mapper->$metaPropertyName = $metaPropertyValue;
                                $fieldsFilleds[] = $metaPropertyName;
                            }

                        }
                    }

                } else {
                    $mapper->$property = $value;
                }

                $fieldsFilleds[] = $property;

            }

        }

        $allFilled = count($fieldsFilleds) === count($fields);

        if ($allFilled) {

            if ($mapper->id !== null) {
                if ($mapper->preferSlug === null && $mapper->name !== null) {
                    $mapper->preferSlug = self::getEncryptIDForSlug($mapper->id);
                    $mapper->update();
                }
            }

        }

        return $allFilled ? $mapper : null;

    }

    /**
     * @return bool
     */
    public static function jsonExtractExistsMySQL()
    {

        try {

            $json = [
                'ok' => true,
            ];
            $json = json_encode($json);
            $sql = "SELECT JSON_EXTRACT('{$json}'" . ', \'$.test\')';
            $prepared = self::model()->prepare($sql);
            $prepared->execute();
            return true;

        } catch (\PDOException $e) {

            if ($e->getCode() == 1305 || $e->getCode() == 42000) {
                return false;
            } else {
                throw $e;
            }

        }

    }

    /**
     * @return ActiveRecordModel
     */
    public static function model()
    {
        return (new OrganizationMapper)->getModel();
    }
}
