# Introducción

![Screenshot](./statics/logo-piecesphp.svg)

Es un framework de desarrollo web es una biblioteca de software que permite a los desarrolladores construir aplicaciones y sitios web de manera eficiente y rápida; esta documentación introduce el despliegue y la utilización de **PiecesPHP**, un framework basado como su nombre lo indica en PHP (probado desde PHP 7.4 hasta 8.2) que no solo facilita el desarrollo rápido de aplicaciones web, sino que también incorpora características modulares avanzadas que abordan las necesidades modernas de escalabilidad, seguridad y rendimiento. 

## Consideraciones iniciales

PiecesPHP requiere el depsliegue de un entorno LAMP, que es el acrónimo usado para describir un sistema de infraestructura de internet el cual usa las siguientes herramientas:

* Linux, el sistema operativo
* Apache, el servidor web
* MySQL/MariaDB, el gestor de bases de datos
* PHP, el lenguaje de programación

A continuación, se proporciona una guía práctica para su implementación y puesta en funcionamiento en Ubuntu (versión 22.04 LTS)

[Instalación de entorno LAMP](./lamp/index.md){ .md-button }

> Aunque gracias a WSL es posible el despliegue en sistemas windows, para más información visite los siguientes enlaces:

- *[¿Qué es WSL?](https://learn.microsoft.com/es-es/windows/wsl/about)* 
- *[Instalar WSL en Windows 11](https://learn.microsoft.com/es-es/windows/wsl/install)*
- *[Instalar Ubuntu en Windows 11](https://canonical-ubuntu-wsl.readthedocs-hosted.com/en/latest/guides/install-ubuntu-wsl2/)*

