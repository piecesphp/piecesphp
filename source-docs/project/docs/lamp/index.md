# LAMP

La siguiente guia fue escrita para la versión de Ububtu 22.04 LTS, para versiones anteriores se debe revisar los reposotorios correspondientes a cada versión.

Pude validar la versión de Ubuntu instalada así:

```bash
lsb_release -a
```

Se recomienda realizar la instalación de los componentes en el siguiente orden:

### [Apache2](./content/Apache.md)
### [PHP](./content/PHP.md)
### [MariaDB](./content/MariaDB.md)

## Componentes / Módulos adicionales

### [PHPMyAdmin](./content/PHPMyAdmin.md)
### [Adminer](./content/Adminer.md) (alternativa ligera a PHPMyAdmin)
### [SSL con Lets Encrypt](./content/SSL con Lets Encrypt.md) (Probado en Debian 9)
### [Virtual Hosts - Soporte para dominios](./content/Virtual Hosts.md) (Probado en Debian 9)


