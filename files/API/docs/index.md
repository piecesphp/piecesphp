# API

_Para las rutas que requieren autorización es necesario enviar la cabecera JWTAuth con el valor del token de autenticación_

### [Publications](./modules/Publications.md)
### [News](./modules/News.md)
### [Usuarios y autenticación](./modules/Usuarios.md)
### [Ubicaciones](./modules/Ubicaciones.md)
